import styled from "styled-components"

export const Container = styled.div`
	padding: 1rem;
	font-family: monospace;
	text-align: center;

	h2 {
		font-size: 2rem;
		font-weight: normal;

		span {
			font-weight: bold;
		}
	}

	div {
		padding: 1rem;

		input,
		button {
			padding: 0.5rem 1rem;
		}

		input {
			margin-right: 1rem;
		}

		button {
			cursor: pointer;
		}
	}
`
