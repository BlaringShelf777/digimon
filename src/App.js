import DigimonList from "./components/DigimonList/idnex"
import Search from "./components/Search"
import { Container } from "./styles/app"

function App() {
	return (
		<Container>
			<section>
				<Search />
				<div>
					<DigimonList />
				</div>
			</section>
		</Container>
	)
}

export default App
