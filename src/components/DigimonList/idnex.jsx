import { useSelector } from "react-redux"
import Digimon from "../Digimon"
import { Container } from "./styles"

const DigimonList = () => {
	const digimons = useSelector((store) => store.digimons)

	return (
		<Container>
			{digimons.map((d) => (
				<Digimon key={d.name} digimon={d} />
			))}
		</Container>
	)
}

export default DigimonList
