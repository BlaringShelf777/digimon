import { Container } from "./styles"

const Digimon = ({ digimon: { name, img, level } }) => {
	return (
		<Container>
			<img src={img} alt={name} />
			<h3>{name}</h3>
			<p>{level}</p>
		</Container>
	)
}

export default Digimon
