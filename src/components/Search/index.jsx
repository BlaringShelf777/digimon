import { useEffect } from "react"
import { useState } from "react"
import { useDispatch } from "react-redux"
import { toast } from "react-toastify"
import addDigimonsThunk from "../../store/modules/digimons/thunk"
import { Container } from "./styles"

const Search = () => {
	const [input, setInput] = useState("")
	const [error, setError] = useState({ status: false, message: "" })
	const dispatch = useDispatch()

	const handleSubmit = () => {
		dispatch(addDigimonsThunk(input, setError))
		setInput("")
	}

	useEffect(() => {
		if (error.status) {
			toast.error(error.message)
			setError({ status: false, message: "" })
		}
	}, [error])

	return (
		<Container>
			<h2>
				Search for your <span>Digimon</span>!
			</h2>
			<div>
				<input
					onChange={(e) => setInput(e.target.value)}
					type="text"
					value={input}
					placeholder="Digimon name here..."
				/>
				<button onClick={handleSubmit}>add digimon</button>
			</div>
		</Container>
	)
}

export default Search
