import api from "../../../services/api"
import { addDigimon } from "./actions"

const addDigimonsThunk = (digimon, setError) => (dispatch, getState) => {
	const { digimons } = getState()

	if (digimons.find((d) => d.name === digimon)) {
		setError({ status: true, message: "Digimon already added!" })
	} else {
		api
			.get(`/api/digimon/name/${digimon}`)
			.then((resp) => {
				dispatch(addDigimon(resp.data[0]))
			})
			.catch((_) => setError({ status: true, message: "Digimon not found!" }))
	}
}

export default addDigimonsThunk
