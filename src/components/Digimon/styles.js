import styled from "styled-components"

export const Container = styled.div`
	background-color: white;
	padding: 1rem;
	font-family: monospace;
	display: flex;
	flex-direction: column;
	align-items: center;
	width: 200px;
	border-radius: 10px;

	img {
		max-width: 150px;
		margin: 0 auto;
	}

	h3 {
		font-size: 1.5rem;
	}
`
